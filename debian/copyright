Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Joel Barrios <darkshram@gmail.com>
Upstream-Name: MATE Eiciel
Source: https://github.com/darkshram/mate-eiciel

Files: ABOUT-NLS
 AUTHORS
 ChangeLog
 Makefile.am
 NEWS
 README
 README.md
 TODO
 autogen.sh
 config.hpp.in
 configure.ac
 doc/C/Makefile.am
 doc/C/add.page
 doc/C/concepts.page
 doc/C/default.page
 doc/C/edit.page
 doc/C/figures/Makefile.am
 doc/C/index.page
 doc/C/mask.page
 doc/C/open.page
 doc/C/remove.page
 doc/C/requirements.page
 doc/C/view.page
 doc/C/xattr.page
 doc/Makefile.am
 doc/C/figures/acl_entry.png
 doc/C/figures/acl_permissions.png
 doc/C/figures/mask_detail.png
 doc/C/figures/open_file.png
 doc/C/figures/open_file_nautilus.png
 doc/C/figures/screen_xattr.png
 doc/C/figures/ugo_permissions.png
 img/group-acl-default.png
 img/group-acl.png
 img/group-default.png
 img/group.png
 img/icon_eiciel_16.png
 img/icon_eiciel_24.png
 img/icon_eiciel_32.png
 img/icon_eiciel_48.png
 img/icon_eiciel_64.png
 img/icon_eiciel_96.png
 img/mask-default.png
 img/mask.png
 img/others-default.png
 img/others.png
 img/user-acl-default.png
 img/user-acl.png
 img/user-default.png
 img/user.png
 img/Makefile.am
 img/icon_eiciel.svg
 man/Makefile.am
 man/mate-eiciel.1
 po/ChangeLog
 po/LINGUAS
 po/Makevars
 po/POTFILES.in
 po/Rules-quot
 po/boldquot.sed
 po/en@boldquot.header
 po/en@quot.header
 po/insert-header.sin
 po/quot.sed
 po/remove-potcdate.sin
 po/stamp-po
 src/Makefile.am
 src/org.mate-desktop.mate-eiciel.desktop.in
Copyright: 2004-2014, Roger Ferrer Ibáñez
License: GPL-2+
Comment:
 Files lack copyright headers. Assuming license and as found in COPYING
 file, assuming main copyright holder of the other code files as
 copyright holder for these files.

Files: src/acl_element_kind.hpp
 src/acl_list.hpp
 src/acl_manager.cpp
 src/acl_manager.hpp
 src/cellrenderer_acl.cpp
 src/cellrenderer_acl.hpp
 src/eiciel_caja_page.cpp
 src/eiciel_caja_page.hpp
 src/eiciel_container.cpp
 src/eiciel_container.hpp
 src/eiciel_main_controller.cpp
 src/eiciel_main_controller.hpp
 src/eiciel_main_window.cpp
 src/eiciel_main_window.hpp
 src/eiciel_standalone.cpp
 src/eiciel_xattr_window.cpp
 src/eiciel_xattr_window.hpp
 src/participant_list.hpp
 src/xattr_list_model.hpp
 src/xattr_manager.cpp
 src/xattr_manager.hpp
Copyright: 2004-2014, Roger Ferrer Ibáñez
License: GPL-2+

Files: po/ar.po
 po/cs.po
 po/de.po
 po/es.po
 po/fr.po
 po/hu.po
 po/it.po
 po/ja.po
 po/ko.po
 po/nl.po
 po/pl.po
 po/pt_BR.po
 po/ru.po
 po/sv.po
 po/zh_CN.po
 po/zh_TW.po
Copyright: 2006, SuSE Linux Products GmbH, Nuernberg
License: GPL-2+

Files: compile
 depcomp
 missing
Copyright: 1996-2014, Free Software Foundation, Inc.
  1999-2014, Free Software Foundation, Inc.
License: GPL-2+

Files: src/eiciel_xattr_controller.cpp
 src/eiciel_xattr_controller.hpp
Copyright: 2004-2014, Roger Ferrer Ibáñez
License: GPL-2+

Files: po/en_GB.po
 po/en_US.po
Copyright: 2006, SUSE Linux GmbH, Nuremberg
License: GPL-2+

Files: Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSF-unlimited

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2013, Free Software Foundation,
License: FSF-All-Permissive

Files: po/Makefile.in.in
Copyright: 1995-1997, 2000-2007, 2009-2010, Ulrich Drepper <drepper@gnu.ai.mit.edu>
License: po-Makefile

Files: ltmain.sh
Copyright: 1996-2001, 2003-2011, Free Software Foundation, Inc.
License: GPL-2+

Files: install-sh
Copyright: 1994, X Consortium
License: MIT~X11

Files: src/org.mate-desktop.mate-eiciel.appdata.xml
Copyright: 2014, Roger Ferrer <rofirrim@gmail.com>
  2017, Joel Barrios <darkshram@gmail.com>
License: GPL-2+

Files: po/eiciel.pot
Copyright: YEAR, Free Software Foundation, Inc.
License: GPL-2+

Files: po/ca.po
Copyright: Roger Ferrer Ibáñez
License: GPL-2+

Files: debian/*
Copyright: 2017, Simon Quigley <tsimonq2@ubuntu.com>
  2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: FSF-All-Permissive
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved.  This file is offered as-is, without
 warranty of any kind.

License: po-Makefile
 This file can be copied and used freely without restrictions.  It can
 be used in projects which are not available under the GNU General Public
 License but which still want to provide support for the GNU gettext
 functionality.
 Please note that the actual code of GNU gettext is covered by the GNU
 General Public License and is *not* in the public domain.

License: MIT~X11
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that copyright
 notice and this permission notice appear in supporting documentation, and
 that the name of the copyright holders not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission.  The copyright holders make no representations
 about the suitability of this software for any purpose.  It is provided "as
 is" without express or implied warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THIS SOFTWARE.

# Copyright (C) 1994-2014 Free Software Foundation, Inc.

License: FSF-unlimited
 This Makefile.in is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.
